#! /usr/bin/python
# -*- coding: utf-8 -*-

##
# usbdir.py
# Devuelve la ruta completa del directorio en donde se encuentra montado el dispositivo de almacenamiento extraible
##
# 
# Autores: 
#   Aguilar Pérez Paulina
#   Lechuga López Kevin Martín
#   Meza Ortega Fernando


import os   #Para el comando y recoger la salida

def get_usb_dir():

    # Vamos a encontrar dispositivos extraibles,
    # asumimos que el dispositivo extraible se montó en el directorio media/
    devices = os.popen('df -Th | grep media').readlines()

    # Lista para guardar la ruta de los dispositivos
    usbs=[]

    # De los dispositivos encontrados, tomaremos su ruta y la guardaremos en usbs
    for dev in devices:
        pos = dev.find("%")
        loc = dev[pos+2:-1]
        usbs.append(loc)

    # Si se detectó más de un dispositivo montado, se le dará al usuario a escoger cual utilizar
    # y se mandará un "0" a elección
    if len(usbs) > 1:
        eleccion = "0"
        print("Escoge un dispositivo extraible:")
        for i in range(len(usbs)):
            print(i+1, ") ",usbs[i])

        d = int(input("Ingresa el numero: "))
        usb_dir = usbs[d-1]

    # Si no hay un dispisitivo USB se mandará un "1" a elección
    elif len(usbs) == 0:
        eleccion = "1"
        usb_dir = ""

    # Si se detectó únicamente un dispositov montado en media/, este se usará por default
    elif len(usbs) == 1:
        eleccion = "0"
        usb_dir = usbs[0]

    return usb_dir, eleccion
#! /usr/bin/python
# -*- coding: utf-8 -*-

##
# main.py
# Funciones relacionadas con el control de flujo del programa así como muchas funciones auxiliares para el manejo de las ventanas
##
# 
# Autores: 
#   Aguilar Pérez Paulina
#   Lechuga López Kevin Martín
#   Meza Ortega Fernando

# Se utiliza para las lecturas de los argumentos
import sys
# Para obtenner informacion sobre los directorios
import os

# Para darle tiempo a VLC
import time

# Para las interfaz de usuario
from tkinter import *

# Creo que se necesitan hilos para poder atender las peticiones del usuario
import threading

# Funciones auxiliares para el medio_extraible
from medio_extraible import detectar_archivos, ReproductorMusica, detectar_archivos_mixto

# Para la música utilizaremos pygame
import pygame

# Para las imagenes: 
from PIL import Image, ImageTk

# Para conseguir la ruta del dispositivo USB de almacenamiento
from usbdir import get_usb_dir

# Cualquiera que lea este código, sea o no el profesor, estoy seguro que es el peor
# diseño de código jamás antes visto, puede que tenga alguna ventaja que no me es visible
# pero así parece ensamblador

# Webbrowser para sustituir spotify y netflix :c
import webbrowser

class Centro_Multimedia:

    def definir_nombres(self):
        self.nombres = {key : value.__name__ for (key, value) in self.menu.items()}

    def exit(self):
        self.salir = True

    def musica_USB(self, usb_dir):
        counter = 0
        lista = []

        for raiz, _ , archivos in os.walk(os.path.abspath(usb_dir)):
            for archivo in archivos:
                if archivo.lower().endswith((".mp3", ".wav")):
                    lista.append(os.path.join(raiz, archivo))
                    counter += 1 
        
        root = Tk()

        app = ReproductorMusica(root)

        musicaactual = threading.Thread(target=app.controlador, args=(lista,))

        musicaactual.start()

        mixer = app.mixer

        print ("Las canciones que serán reproducidas ciclicamente son: ")
        for n, elemento in enumerate(lista):
            print(str(n) + " -> " + elemento)

        root.mainloop()

        if mixer:
            mixer.quit()

        return 

    # Función que asigna las imagenes de la lista images y se repite hasta que acabe el mainloop
    def presentacion(self):                                                     
        if self.i >= (len(self.images)-1):
            self.i = 0
            self.imagen.config(image = self.images[self.i])
        else:
            self.i = self.i + 1
            self.imagen.configure(image = self.images[self.i])
        self.pantalla = self.imagen.after(2000 , self.presentacion)

    def imagenes_USB(self, usb_dir):

        # Creamos una ventana para mostrar las imagenes
        self.root = Tk()
        self.root.title("Centro Multimedia: Presentación")
        self.root.geometry("800x600")
        self.root.config(bg="black")
        self.root.resizable(0, 0)

        # Buscamos archivos que sean imágenes para guardar su ruta absoluta en files
        files = []
        for raiz, _ , archivos in os.walk(os.path.abspath(usb_dir)): 
            for archivo in archivos: 
                if archivo.lower().endswith((".png", ".jpeg", ".jpg")): 
                    files.append(os.path.join(raiz, archivo))

        # Con las rutas, creamos imágenes y las agregamos a una lista para hacer la presentación
        self.images= []
        for f in files:
            ima = Image.open(r''+f)
            w, h = ima.size
            new_width, new_height = self.ratio(800, 600, w, h)
            self.images.append(ImageTk.PhotoImage(ima.resize((int(new_width), int(new_height)))))

        # Asignamos la imagen a una label para mostrarla en la ventana
        self.i = 0
        self.imagen = Label(self.root , image=self.images[self.i])
        self.imagen.pack(pady=50)

        self.presentacion()
        self.root.mainloop()

        return


    def videos_USB(self, usb_dir):
        print("Modulo por desarrollar :c")
        pass

    def mixto_USB(self, usb_dir):
        print("El usb tiene contenido mixto, ¿Qué quieres reproducir?")
        tupla = detectar_archivos_mixto(usb_dir)
        tempmenu = {'0' : self.musica_USB, '1': self.imagenes_USB, '2' : self.videos_USB}
        self.menu = {}
        for x, y in tempmenu.items():
            if (tupla[int(x)]):
                self.menu[x] = y 
        self.definir_nombres()

        self.print_menu()

        eleccion = input("Elige una opcion: ")
        self.menu[eleccion](usb_dir)

    def no_archivos_USB(self, usb_dir):
        print("No existen archivos compatibles para ser reproducidos")
        return

    def lectura_exitosa_USB(self, usb_dir):

        self.menu = {'0' : self.musica_USB, '1': self.imagenes_USB, '2' : self.videos_USB, 
            '3' : self.mixto_USB,'4' : self.no_archivos_USB}

        self.definir_nombres()

        eleccion = detectar_archivos(usb_dir)

        self.menu[eleccion](usb_dir)

        return

    def lectura_fallida_USB(self, usb_dir):
        print("No se pudo leer la USB")
        return

    # Funcion para ajustar el tamaño de la imagen
    def medio_extraible(self):
        self.menu = {'0' : self.lectura_exitosa_USB, '1': self.lectura_fallida_USB}
        self.definir_nombres()

        # Codigo para detectar USB:
        usb_dir, eleccion = get_usb_dir()
        print(usb_dir)

        self.menu[eleccion](usb_dir)

        return

    def ratio(self, w, h, aspect_w, aspect_h):

        new_width = 0
        new_height = 0

        # ¿Si puedo mostrar la imagen o no?
        if aspect_w <= w and aspect_h <= h:
            new_width = aspect_w
            new_height = aspect_h
        else:
            # Cuidado, flotantes
            relacion_de_aspecto_imagen = aspect_w / aspect_h
            relacion_de_aspecto_frame = w / h

            # Frame es más ancho que la imagen en relación de aspecto
            if relacion_de_aspecto_frame >= relacion_de_aspecto_imagen:
                # La imagen debe de tener el alto del frame
                new_height = h
                new_width = h * relacion_de_aspecto_imagen
                
            # Frame es más alto que la imagen en relación de aspecto
            else:
                # La imagen debe de tener el ancho del frame
                new_width = w
                new_height = w * 1/relacion_de_aspecto_imagen

        return new_width, new_height 


    def netflix(self):
        webbrowser.open("https://www.netflix.com/browse")
        return

    def spotify(self):
        webbrowser.open("https://open.spotify.com/")
        return
   
    def print_menu(self, *args):
        for n, y in self.nombres.items():
            print(int(n), y)
        for arg in args:
            print(arg)

    def mainLoop(self):
        self.salir = False

        while not self.salir:
            self.menu = {'0' : self.netflix, '1' : self.spotify , '2': self.medio_extraible, '3': self.exit}
            self.definir_nombres()

            self.print_menu()
            eleccion = input("Elige una opcion: ")
            self.menu[eleccion]()

if __name__ == "__main__":
    app = Centro_Multimedia()
    app.mainLoop()
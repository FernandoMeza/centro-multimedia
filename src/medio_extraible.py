#! /usr/bin/python
# -*- coding: utf-8 -*-

##
# medio_extraible.py
# Funciones auxiliares para la música y para detectar archivos bien
##
# 
# Autores: 
#   Aguilar Pérez Paulina
#   Lechuga López Kevin Martín
#   Meza Ortega Fernando

import os

from tkinter import *
import pygame
from PIL import Image, ImageTk



# Los formatos que toleramos son pocos, para facilitarnos las cosas

def detectar_archivos(usb_dir):
    archivos_musica = 0
    archivos_imagenes = 0
    archivos_videos = 0
    for raiz, directorios, archivos in os.walk(os.path.abspath(usb_dir)):
        for archivo in archivos:
            print(archivo)
            if archivo.lower().endswith((".mp3", ".wav")):
                archivos_musica += 1
            elif archivo.lower().endswith((".png", ".jpg", ".jpeg")):
                archivos_imagenes += 1
            elif archivo.lower().endswith((".mp4", ".mvk")):
                archivos_videos += 1

    # Musica
    if archivos_musica > 0 and archivos_imagenes == 0 and archivos_videos == 0:
        return '0'
    # Imagenes
    elif archivos_imagenes > 0 and archivos_musica == 0 and archivos_videos == 0:
        return '1'
    # Videos
    elif archivos_videos > 0 and archivos_imagenes == 0 and archivos_musica == 0:
        return '2'
    # Mixto
    elif archivos_imagenes > 0 or archivos_musica > 0 or archivos_videos > 0:
        return '3'
    # No compatible
    else:
        return '4'

# Ya sé que es mixto
def detectar_archivos_mixto(usb_dir):
    archivos_musica = 0
    archivos_imagenes = 0
    archivos_videos = 0
    for raiz, directorios, archivos in os.walk(os.path.abspath(usb_dir)):
        for archivo in archivos:
            if archivo.lower().endswith((".mp3", ".wav")):
                archivos_musica += 1
            elif archivo.lower().endswith((".png", ".jpg", ".jpeg")):
                archivos_imagenes += 1
            elif archivo.lower().endswith((".mp4", ".mvk")):
                archivos_videos += 1

    return True if archivos_musica > 0 else False, True if archivos_imagenes > 0 else False, True if archivos_videos > 0 else False


class ReproductorMusica:
    def __init__(self, ventana):
        self.ventana = ventana

        self.ventana.geometry("200x150")
        self.ventana.title("Reproductor de musica")
        self.ventana.resizable(0,0)
        self.ventana.grid_rowconfigure(0, weight=1)
        self.ventana.grid_columnconfigure(0, weight=1)

        self.Regresar = Button(self.ventana, text = "Salir", command = self.exit)

        self.Regresar.grid(row=0, column=0, sticky="nsew")

        self.archivo = False
        self.reproduciendo = False
        self.posicion = 0

        pygame.mixer.init()
        pygame.init()
        self.mixer = pygame.mixer

        self.mixer.fadeout(1000)

    def exit(self):
        self.mixer.music.stop()
        self.mixer.music.unload()
        self.mixer.quit()
        self.ventana.destroy()

    def controlador(self, lista_de_canciones):
        try:
            self.lista_de_canciones = lista_de_canciones
            self.tam = len(self.lista_de_canciones)
            self.eleccion = 0
            self.mixer.music.load(self.lista_de_canciones[self.eleccion % self.tam])
            self.mixer.music.play(loops=1, fade_ms=1000)
            print("Reproduciendo:", self.lista_de_canciones[self.eleccion % self.tam])
            while True:
                if self.mixer.music.get_busy():
                    pass
                else:
                    self.eleccion += 1
                    self.mixer.music.load(self.lista_de_canciones[self.eleccion % self.tam])
                    self.mixer.music.play(loops=1, fade_ms=1000)
                    print("Reproduciendo:", self.lista_de_canciones[self.eleccion % self.tam])
        except:
            pass